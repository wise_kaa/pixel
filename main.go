package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"time"

	"gitlab.com/wise_kaa/pixel/configuration"
	"gitlab.com/wise_kaa/pixel/entities"
)

func init() {

	configuration.InitConfig()

	entities.UnitTypePool = append(entities.UnitTypePool, entities.UnitType{
		Name:        "Luchnik",
		Speed:       2,
		Damage:      1,
		StartHealth: 10,
		Range:       5,
	})

	entities.UnitTypePool = append(entities.UnitTypePool, entities.UnitType{
		Name:        "Mechnik",
		Speed:       1,
		Damage:      2,
		StartHealth: 10,
		Range:       2,
	})
}

func main() {
	fmt.Println("Press Space to start game.")
	fmt.Println("Press Esc to exit game.")

	ch := make(chan byte)
	go func(ch chan byte) {
		// disable input buffering
		exec.Command("stty", "-F", "/dev/tty", "cbreak", "min", "1").Run()
		// do not display entered characters on the screen
		exec.Command("stty", "-F", "/dev/tty", "-echo").Run()
		var b []byte = make([]byte, 1)
		for {
			os.Stdin.Read(b)
			ch <- b[0] // читаем код символа в канал
		}
	}(ch)

	for {
		select {
		case keyCode := <-ch:
			if keyCode == 27 {
				return
			}

			if keyCode == 32 {
				fmt.Println("GO!")
				Run()
				fmt.Println("Press Space to start game.")
				fmt.Println("Press Esc to exit game.")
			}
		}
		time.Sleep(time.Millisecond * 100)
	}
}

func Run() {
	chEnd := make(chan bool)
	entities.GameFinished = false

	fmt.Printf("Формирование игрового поля %d на %d \n", configuration.Config.Game.GamefieldX, configuration.Config.Game.GamefieldY)
	fmt.Printf("Участвуют %d комманд по %d юнитов \n", configuration.Config.Game.TeamsNumber, configuration.Config.Game.TeamUnitsNumber)

	// заполнение командами поля
	entities.UnitPool = []*entities.Unit{}
	unitID := 0
	typesLen := len(entities.UnitTypePool)
	for team := 1; team <= configuration.Config.Game.TeamsNumber; team++ {
		for unit := 1; unit <= configuration.Config.Game.TeamUnitsNumber; unit++ {
			// set random unit type
			rand.Seed(time.Now().UnixNano())
			unitTypeRandomIndex := rand.Intn(typesLen)
			unitType := entities.UnitTypePool[unitTypeRandomIndex]

			// set random unit position on left/right side
			position := entities.Position{X: 0, Y: 0}
			if team%2 == 0 {
				position.X = configuration.Config.Game.GamefieldX
			}
			position.Y = rand.Intn(configuration.Config.Game.GamefieldY + 1)

			unitID++
			Unit := entities.Unit{
				ID:       unitID,
				Team:     team,
				UnitType: unitType,
				Position: position,
				Health:   unitType.StartHealth,
			}

			entities.UnitPool = append(entities.UnitPool, &Unit)

		}
	}

	for _, unit := range entities.UnitPool {
		fmt.Printf("В игре участвует юнит %d типа %s за команду %d на позиции %d %d \n", unit.ID, unit.UnitType.Name, unit.Team, unit.Position.X, unit.Position.Y)
		go unit.Fight(chEnd) // юнит начинает искать цель и атаковать
	}

	fmt.Println("В Бой!")

	<-chEnd
	entities.GameFinished = true

	fmt.Println("Конец игры")
}
