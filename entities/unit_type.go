package entities

type UnitType struct {
	Name        string // название типа юнита
	Speed       int    // скорость юнита
	Damage      int    // урон
	StartHealth int    // Стартовое здоровье
	Range       int    // дальность, при которой может наноситься урон
}
