package entities

import (
	"fmt"
	"math"
	"time"
)

type Unit struct {
	ID         int
	Team       int
	UnitType   UnitType
	Position   Position
	Health     int
	TargetUnit *Unit
}

type Position struct {
	X int
	Y int
}

func (unit *Unit) Fight(chEnd chan bool) {
	for {
		if unit.Health < 1 || GameFinished { // юнит убит или конец игры
			break
		}

		if !unit.FindTarget() { // если противников больше не найдено, завершим игру
			chEnd <- true
			break
		}

		unit.MoveToTarget()

		unit.AttackTarget()

		time.Sleep(1 * time.Second)
	}

}

func (unit *Unit) FindTarget() bool {
	if unit.Health < 1 { // юнит убит
		return false
	}

	if unit.TargetUnit != nil { // у юнита уже есть цель, выходим
		return true
	}

	var closestTargetUnit *Unit // ближайший противник
	var distance float64
	for _, targetUnit := range UnitPool {
		// пропускаем убитых
		if targetUnit.Health < 1 {
			continue
		}

		// пропускием своих
		if targetUnit.Team == unit.Team {
			continue
		}

		if closestTargetUnit == nil {
			closestTargetUnit = targetUnit
			unit.TargetUnit = closestTargetUnit
			distance = unit.GetDistanceToTarget(*closestTargetUnit)
			continue
		}

		if unit.GetDistanceToTarget(*targetUnit) < distance { // ближайший противник
			unit.TargetUnit = targetUnit
		}
	}

	if unit.TargetUnit == nil {
		fmt.Printf("Противников больше нет, победила команда %d \n", unit.Team)

		// посчитаем оставшихся в живых
		n := 0
		for _, v := range UnitPool {
			if v.Health > 0 {
				n++
			}
		}
		fmt.Printf("В живых сталось %d юнитов \n", n)

		return false
	}

	fmt.Printf("Юнит %d захватил цель %d \n", unit.ID, unit.TargetUnit.ID)
	return true
}

func (unit *Unit) GetDistanceToOwnTarget() float64 {
	x := unit.Position.X - unit.TargetUnit.Position.X
	y := unit.Position.Y - unit.TargetUnit.Position.Y

	distance := math.Sqrt(float64(x*x + y*y))

	return distance
}

func (unit *Unit) GetDistanceToTarget(targetUnit Unit) float64 {
	x := unit.Position.X - targetUnit.Position.X
	y := unit.Position.Y - targetUnit.Position.Y

	distance := math.Sqrt(float64(x*x + y*y))

	return distance
}

func (unit *Unit) MoveToTarget() {
	if unit.GetDistanceToOwnTarget() < float64(unit.UnitType.Range) { // юнит достаточно близко к цели, передвижение не нужно
		return
	}

	kx, ky := -1, -1
	x := unit.Position.X - unit.TargetUnit.Position.X
	y := unit.Position.Y - unit.TargetUnit.Position.Y

	// модуль x
	if x < 0 {
		x = -x
		kx = 1
	}
	// модуль y
	if y < 0 {
		y = -y
		ky = 1
	}
	if x > y {
		unit.Position.X += unit.UnitType.Speed * kx
	} else {
		unit.Position.Y += unit.UnitType.Speed * ky
	}

	fmt.Printf("Юнит %d передвинулся к цели %d, теперь его координаты %d, %d \n", unit.ID, unit.TargetUnit.ID, unit.Position.X, unit.Position.Y)
}

func (unit *Unit) AttackTarget() {
	if unit.GetDistanceToOwnTarget() > float64(unit.UnitType.Range) { // цель убежала
		return
	}

	if unit.TargetUnit.Health < 1 { // цель уже убита другим
		unit.TargetUnit = nil // обнулим цель юнита
		return
	}

	unit.TargetUnit.Health -= unit.UnitType.Damage

	fmt.Printf("Юнит %d атакует цель %d, здоровье у цели %d \n", unit.ID, unit.TargetUnit.ID, unit.TargetUnit.Health)

	if unit.TargetUnit.Health < 1 {
		fmt.Printf("Юнит %d убил цель %d \n", unit.ID, unit.TargetUnit.ID)
		unit.TargetUnit = nil
	}

}
