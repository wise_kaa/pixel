package configuration

import (
	"gopkg.in/yaml.v2"
	"log"
	"os"
)

var Config = struct {
	Game struct {
		GamefieldX      int `yaml:"game_field_x"`
		GamefieldY      int `yaml:"game_field_y"`
		TeamsNumber     int `yaml:"teams_number"`
		TeamUnitsNumber int `yaml:"team_units_number"`
	}
}{}

func InitConfig() {

	f, err := os.Open("./configuration/game.yml")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&Config)
	if err != nil {
		log.Fatal(err)
	}
}
